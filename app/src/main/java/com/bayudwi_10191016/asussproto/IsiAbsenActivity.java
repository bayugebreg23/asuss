package com.bayudwi_10191016.asussproto;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class IsiAbsenActivity extends AppCompatActivity {
    EditText kelas, waktusiswa, aktivitas, ketpresensi;
    Button submitabsen;

    DatabaseReference database = FirebaseDatabase.getInstance().getReference();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_isi_absen);

        kelas = findViewById(R.id.kelas);
        waktusiswa = findViewById(R.id.waktusiswa);
        aktivitas = findViewById(R.id.aktivitas);
        ketpresensi = findViewById(R.id.ketpresensi);
        submitabsen = findViewById(R.id.submitabsen);

        submitabsen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String getKelas = kelas.getText().toString();
                String getWaktusiswa = waktusiswa.getText().toString();
                String getAktivitas = aktivitas.getText().toString();
                String getKetpresensi = ketpresensi.getText().toString();

                if(getKelas.isEmpty()){
                    kelas.setError("Masukkan Kelas / Mata Kuliah");
                } else if (getWaktusiswa.isEmpty()){
                    waktusiswa.setError("Masukkan Waktu Kehadiran");
                } else if (getAktivitas.isEmpty()){
                    aktivitas.setError("Masukkan Aktivitas Anda");
                } else if (getKetpresensi.isEmpty()){
                    ketpresensi.setError("Masukkan Keterangan hadir Anda");
                } else {
                    database.child("Presensi").push().setValue(new ModelAbsensi(getKelas, getWaktusiswa, getAktivitas, getKetpresensi)).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void unused) {
                            Toast.makeText(IsiAbsenActivity.this, "Presensi Berhasil", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(IsiAbsenActivity.this, AbsenActivity.class));
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(IsiAbsenActivity.this, "Presensi Anda Gagal", Toast.LENGTH_SHORT).show();
                        }
                    });
                    database.child("Presensi").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });
                }
            }
        });
    }

}