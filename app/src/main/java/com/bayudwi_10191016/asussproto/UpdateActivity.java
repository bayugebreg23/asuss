package com.bayudwi_10191016.asussproto;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class UpdateActivity extends AppCompatActivity {
    EditText nama, nomorhp, emailprofil, ultah;
    Button submitprofil;

    DatabaseReference database2 = FirebaseDatabase.getInstance().getReference();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        nama = findViewById(R.id.nama);
        nomorhp = findViewById(R.id.nomorhp);
        emailprofil = findViewById(R.id.emailprofil);
        ultah = findViewById(R.id.ultah);
        submitprofil = findViewById(R.id.submitprofil);

        submitprofil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String getNama = nama.getText().toString();
                String getNomorhp = nomorhp.getText().toString();
                String getEmailprofil = emailprofil.getText().toString();
                String getUltah = ultah.getText().toString();

                if (getNama.isEmpty()) {
                    nama.setError("Masukkan Nama");
                } else if (getNomorhp.isEmpty()) {
                    nomorhp.setError("Masukkan Nomor HP");
                } else if (getEmailprofil.isEmpty()) {
                    emailprofil.setError("Masukkan Email");
                } else if (getUltah.isEmpty()) {
                    ultah.setError("Masukkan Tangggal Lahir");
                } else {
                    database2.child("Profil").push().setValue(new ModelProfile(getNama, getNomorhp, getEmailprofil, getUltah)).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void unused) {
                            Toast.makeText(UpdateActivity.this, "Data berhasil Ditambah", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(UpdateActivity.this, ProfilActivity.class));
                            finish();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(UpdateActivity.this, "Presensi Anda Gagal", Toast.LENGTH_SHORT).show();
                        }
                    });
                    database2.child("Presensi").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });
                }
            }
        });
    }
}