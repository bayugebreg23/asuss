package com.bayudwi_10191016.asussproto;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class AdapterPresensi extends RecyclerView.Adapter<AdapterPresensi.MyViewHolder> {
    private List<ModelAbsensi> mList;
    private Activity activity;
    DatabaseReference database = FirebaseDatabase.getInstance().getReference();

    public AdapterPresensi(List<ModelAbsensi>mList, Activity activity) {
        this.mList = mList;
        this.activity = activity;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View viewItem = inflater.inflate(R.layout.layout_data_absen, parent, false);
        return new MyViewHolder(viewItem);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final ModelAbsensi data = mList.get(position);
        holder.tampilkelas.setText("Kelas : " + data.getKelas());
        holder.tampilwaktu.setText("Waktu Kehadiran : " + data.getWaktusiswa());
        holder.tampilaktivitas.setText("Aktivitas : " + data.getAktivitas());
        holder.tampilketpresensi.setText("Keterangan Presensi : " + data.getKetpresensi());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tampilkelas, tampilwaktu, tampilaktivitas, tampilketpresensi;
        CardView card_hasil;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tampilkelas = itemView.findViewById(R.id.tampilkelas);
            tampilwaktu = itemView.findViewById(R.id.tampilwaktu);
            tampilaktivitas = itemView.findViewById(R.id.tampilaktivitas);
            tampilketpresensi = itemView.findViewById(R.id.tampilketpresensi);
            card_hasil = itemView.findViewById(R.id.card_hasil);

        }
    }
}
