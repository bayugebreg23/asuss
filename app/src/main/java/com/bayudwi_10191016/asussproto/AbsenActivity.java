package com.bayudwi_10191016.asussproto;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class AbsenActivity extends AppCompatActivity {
    TextView isiabsen, dataabsen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_absen);

        isiabsen = (TextView)findViewById(R.id.isiabsen);
        isiabsen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AbsenActivity.this, IsiAbsenActivity.class));
            }
        });

        dataabsen = (TextView)findViewById(R.id.dataabsensi);
        dataabsen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AbsenActivity.this, DataAbsenActivity.class));
            }
        });

    }
}