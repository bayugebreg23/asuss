package com.bayudwi_10191016.asussproto;

public class ModelProfile {
    private String nama;
    private String nomorhp;
    private String emailprofil;
    private String ultah;
    private String key;

    public ModelProfile() {

    }

    public ModelProfile(String nama, String nomorhp, String emailprofil, String ultah) {
        this.nama = nama;
        this.nomorhp = nomorhp;
        this.emailprofil = emailprofil;
        this.ultah = ultah;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNomorhp() {
        return nomorhp;
    }

    public void setNomorhp(String nomorhp) {
        this.nomorhp = nomorhp;
    }

    public String getEmailprofil() {
        return emailprofil;
    }

    public void setEmailprofil(String emailprofil) {
        this.emailprofil = emailprofil;
    }

    public String getUltah() {
        return ultah;
    }

    public void setUltah(String ultah) {
        this.ultah = ultah;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
