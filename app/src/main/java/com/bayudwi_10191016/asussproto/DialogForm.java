package com.bayudwi_10191016.asussproto;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class DialogForm extends DialogFragment {
    String nama, nomorhp, emailprofil, ultah, key, pilih;
    DatabaseReference database2 = FirebaseDatabase.getInstance().getReference();

    public DialogForm(String nama, String nomorhp, String emailprofil, String ultah, String key, String pilih) {
        this.nama = nama;
        this.nomorhp = nomorhp;
        this.emailprofil = emailprofil;
        this.ultah = ultah;
        this.key = key;
        this.pilih = pilih;
    }

    TextView tnama, tnomorhp, temailprofil, tultah;
    Button submitprofil;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.activity_update, container, false);

        tnama = view.findViewById(R.id.nama);
        tnomorhp = view.findViewById(R.id.nomorhp);
        temailprofil = view.findViewById(R.id.emailprofil);
        tultah = view.findViewById(R.id.ultah);
        submitprofil = view.findViewById(R.id.submitprofil);

        tnama.setText(nama);
        tnomorhp.setText(nomorhp);
        temailprofil.setText(emailprofil);
        tultah.setText(ultah);
        submitprofil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nama = tnama.getText().toString();
                String anomorhp = tnomorhp.getText().toString();
                String aemailprofil = temailprofil.getText().toString();
                String aultah = tultah.getText().toString();
                if (pilih.equals("Ubah")) {
                    database2.child("Profil").child(key).setValue(new ModelProfile(nama, anomorhp, aemailprofil, aultah)).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void unused) {
                            Toast.makeText(view.getContext(), "Data Berhasil di Update", Toast.LENGTH_SHORT).show();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(view.getContext(), "Gagal Mengupdate Data", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }
}
