package com.bayudwi_10191016.asussproto;

public class ModelAbsensi {
    private String kelas;
    private String waktusiswa;
    private String aktivitas;
    private String ketpresensi;
    private String key;

    public ModelAbsensi() {

    }

    public ModelAbsensi(String kelas, String waktusiswa, String aktivitas, String ketpresensi) {
        this.kelas = kelas;
        this.waktusiswa = waktusiswa;
        this.aktivitas = aktivitas;
        this.ketpresensi = ketpresensi;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getWaktusiswa() {
        return waktusiswa;
    }

    public void setWaktusiswa(String waktusiswa) {
        this.waktusiswa = waktusiswa;
    }

    public String getAktivitas() {
        return aktivitas;
    }

    public void setAktivitas(String aktivitas) {
        this.aktivitas = aktivitas;
    }

    public String getKetpresensi() {
        return ketpresensi;
    }

    public void setKetpresensi(String ketpresensi) {
        this.ketpresensi = ketpresensi;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
