package com.bayudwi_10191016.asussproto;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ProfilActivity extends AppCompatActivity {
    Button updateprofil, logout;

    AdapterProfil AdapterProfil;
    DatabaseReference database2 = FirebaseDatabase.getInstance().getReference();
    ArrayList<ModelProfile> listProfil;
    RecyclerView dataprofil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);

        updateprofil = findViewById(R.id.updateprofil);
        updateprofil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProfilActivity.this, UpdateActivity.class));
            }
        });

        logout = findViewById(R.id.logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(ProfilActivity.this, MainActivity.class));
            }
        });

        dataprofil = findViewById(R.id.dataprofil);
        RecyclerView.LayoutManager mLayout = new LinearLayoutManager(this);
        dataprofil.setLayoutManager(mLayout);
        dataprofil.setItemAnimator(new DefaultItemAnimator());

        tampildataprofil();
    }

    private void tampildataprofil() {
        database2.child("Profil").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                listProfil = new ArrayList<>();
                for (DataSnapshot item : snapshot.getChildren()) {
                    ModelProfile profil = item.getValue(ModelProfile.class);
                    profil.setKey(item.getKey());
                    listProfil.add(profil);
                }
                AdapterProfil = new AdapterProfil(listProfil, ProfilActivity.this);
                dataprofil.setAdapter(AdapterProfil);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}