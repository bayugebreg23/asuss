package com.bayudwi_10191016.asussproto;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class AdapterProfil extends RecyclerView.Adapter<AdapterProfil.MyViewHolder>{
    private List<ModelProfile> nList;
    private Activity activity2;
    DatabaseReference database2 = FirebaseDatabase.getInstance().getReference();

    public AdapterProfil(List<ModelProfile>nList, Activity activity2) {
        this.nList = nList;
        this.activity2 = activity2;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View viewItem = inflater.inflate(R.layout.layout_data_profil, parent, false);
        return new MyViewHolder(viewItem);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final ModelProfile data = nList.get(position);
        holder.tampilnama.setText("Nama : " + data.getNama());
        holder.tampilnomor.setText("Nomor HP : " + data.getNomorhp());
        holder.tampilemail.setText("Email : " + data.getEmailprofil());
        holder.tampilultah.setText("Tanggal Lahir : " + data.getUltah());

        holder.card_hasilprofil.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                FragmentManager manager = ((AppCompatActivity)activity2).getSupportFragmentManager();
                DialogForm dialog = new DialogForm(
                        data.getNama(),
                        data.getNomorhp(),
                        data.getEmailprofil(),
                        data.getUltah(),
                        data.getKey(),
                        "Ubah"
                );

                dialog.show(manager, "form");
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return nList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tampilnama, tampilnomor, tampilemail, tampilultah;
        CardView card_hasilprofil;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tampilnama = itemView.findViewById(R.id.tampilnama);
            tampilnomor = itemView.findViewById(R.id.tampilnomor);
            tampilemail = itemView.findViewById(R.id.tampilemail);
            tampilultah = itemView.findViewById(R.id.tampilultah);
            card_hasilprofil = itemView.findViewById(R.id.card_hasilprofil);

        }
    }
}
